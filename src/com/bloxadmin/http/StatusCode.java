package com.bloxadmin.http;


public enum StatusCode {
    // 2xx
    OK(200, "OK"),
    CREATED(201, "Created"),
    NO_CONTENT(204, "No Content"),
    // 3xx
    // 4xx
    BAD_REQUEST(400, "Bad Request"),
    UNAUTHORIZED(401, "Unauthorized"),
    FORBIDDEN(403, "Forbidden"),
    NOT_FOUND(404, "Not Found"),
    METHOD_NOT_ALLOWED(405, "Method Not Allowed"),
    // 5xx
    INTERNAL_SERVER_ERROR(500, "Internal Server Error");

    private final int code;
    private final String message;

    StatusCode(int i, String message) {
        this.code = i;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getHTTPHead() {
        return getHTTPHead("HTTP/1.1");
    }

    public String getHTTPHead(String protocol) {
        return protocol + " " + code + " " + message.toUpperCase();
    }
}
