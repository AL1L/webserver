package com.bloxadmin.http;

import com.bloxadmin.http.websocket.WebSocket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.*;
import java.nio.channels.ServerSocketChannel;
import java.util.HashMap;

public class Server {

    private HashMap<String, Website> websites = new HashMap<>();
    private Thread thread;
    private final String host;
    private final int port;
    private final InetSocketAddress address;
    boolean stop = false;
    private ServerSocketChannel server;
    private ServerSocket serverSocket;
    private final byte[] EOL = {(byte) '\r', (byte) '\n'};
    private Website defaultSite = new DefaultSite();
    private WebSocket webSocket = null;

    public Server(String host, int port) {
        this(new InetSocketAddress(host, port));
    }

    public Server(InetSocketAddress address) {
        this.port = address.getPort();
        this.host = address.getHostName();
        this.address = address;

        thread = new Thread(() -> {
            if (stop)
                return;
            try {
                server = ServerSocketChannel.open();
                server.bind(address, 50);
                serverSocket = server.socket();
                System.out.println("Listening on http://" + address.getHostName() + ":" + address.getPort() + "/ ...");

                while (!stop && !serverSocket.isClosed()) {
                    Socket socket = serverSocket.accept();
                    handleSocket(socket);
                }
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        });
    }

    private void handleSocket(Socket socket) {
        new Thread(() -> {
            OutputStream os = null;
            InputStream is = null;
            PrintStream ps = null;
            try {
                os = socket.getOutputStream();
                is = socket.getInputStream();
                ps = new PrintStream(os);
                Request request = new Request(serverSocket, socket);
                if (webSocket != null) {
                    if (webSocket.isWebsocket(request)) {
                        webSocket.handle(socket, request);
                        return;
                    }
                }
                String h = request.getRequestUri().getHost();
                if (h.equalsIgnoreCase("localhost") || h.equals("0:0:0:0:0:0:0:1") || h.equals("127.0.0.1"))
                    h = "localhost";
                if (h.equals("0.0.0.0"))
                    h = "*";
                Website website = websites.get(h);
                if (website == null)
                    website = websites.get("*");
                if (website == null)
                    website = defaultSite;
                Response response = website.handle(request);
                if (response == null) {
                    response = new Response();
                    response.setStatusCode(StatusCode.NOT_FOUND);
                }
                response.setHeader("content-length", response.getBody().length);

                ps.print(response.getStatusCode().getHTTPHead());
                ps.write(EOL);
                PrintStream finalPs = ps;
                response.getHeaders().forEach((k, v) -> {
                    finalPs.print(k + ": " + v);
                    finalPs.print("\r\n");
                });
                ps.write(EOL);
                if (request.getMethod() != Method.HEAD)
                    ps.write(response.getBody());
                ps.flush();
            } catch (Exception e) {
                try {
                    if (ps != null) {
                        ps.print("HTTP/1.1 500 INTERNAL ERROR");
                        ps.write(EOL);
                        ps.flush();
                    }
                } catch (IOException ignored) {
                }
                e.printStackTrace();
            } finally {
                try {
                    if (ps != null)
                        ps.flush();
                    if (os != null)
                        os.flush();
                    socket.close();
                } catch (IOException ignored) {

                }
            }
        }).start();
    }

    public Server addWebsite(Website website) {
        for (String h : website.getHosts()) {
            if (h.equalsIgnoreCase("localhost") || h.equals("0:0:0:0:0:0:0:1") || h.equals("127.0.0.1"))
                h = "localhost";
            if (h.equals("0.0.0.0"))
                h = "*";
            websites.put(h, website);
        }
        return this;
    }

    public Server start() {
        this.stop = false;
        thread.start();
        return this;
    }

    public Server stop() {
        stop = true;
        return this;
    }

    public Server forceStop() {
        stop();
        thread.stop();
        return this;
    }

    public void setDefaultSite(Website defaultSite) {
        this.defaultSite = defaultSite;
    }

    public ServerSocket getServerSocket() {
        return serverSocket;
    }

    public ServerSocketChannel getServer() {
        return server;
    }

    public void setWebSocket(WebSocket webSocket) {
        this.webSocket = webSocket;
    }

    public WebSocket getWebSocket() {
        return webSocket;
    }
}
