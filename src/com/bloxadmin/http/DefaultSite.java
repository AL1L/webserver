package com.bloxadmin.http;

import java.io.IOException;

public class DefaultSite implements Website {

    @Override
    public Response handle(Request request) throws IOException {
        Response res = new Response();
        StringBuilder b = new StringBuilder();
        request.getHeaders().forEach((k, v) -> b.append(k).append(": ").append(v).append("\n"));
        res.setBody(b.toString());
        res.setStatusCode(StatusCode.OK);
        res.setHeader("content-type", "text/plain");
        return res;
    }

    @Override
    public String[] getHosts() {
        return new String[0];
    }
}
