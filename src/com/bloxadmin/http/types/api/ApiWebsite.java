package com.bloxadmin.http.types.api;

import com.bloxadmin.http.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ApiWebsite implements Website<ApiResponse> {

    private final String[] hosts;
    private HashMap<List<String>, List<ApiEndpoint>> endpoints = new HashMap<>();
    private boolean userAgentReqired = true;

    public ApiWebsite(String... hosts) {
        this.hosts = hosts;
    }

    @Override
    public ApiResponse handle(Request request) {
        ApiResponse res = new ApiResponse();
        res.set("host", request.getSocket().getInetAddress().getHostName());

        if (userAgentReqired) {
            if (request.getHeader("user-agent") == null) {
                res.setStatusCode(StatusCode.BAD_REQUEST);
                res.setMessage("Missing User-Agent");
                return res;
            } else if (request.getHeader("user-agent").trim().equals("")) {
                res.setStatusCode(StatusCode.BAD_REQUEST);
                res.setMessage("Missing User-Agent");
                return res;
            }
        }

        res.setStatusCode(StatusCode.NOT_FOUND);

        String path = request.getRequestUri().getPath();
        if (path.charAt(0) == '/')
            path = path.substring(1);

        List<String> args = Arrays.asList(path.split("/"));
        List<ApiEndpoint> points = endpoints.get(args);

        if (points != null) {
            boolean did = false;
            List<String> allowedMethods = new ArrayList<>();
            try {
                Method m = request.getMethod();
                if (m == Method.HEAD)
                    m = Method.GET;
                for (ApiEndpoint point : points) {
                    allowedMethods.add(point.getMethod().getName());
                    if (point.getMethod() == m) {
                        if (!did)
                            res.setStatusCode(StatusCode.OK);
                        did = true;
                        point.getRunnable().call(request, res);
                    }
                }
            } catch (Throwable e) {
                res.setStatusCode(StatusCode.INTERNAL_SERVER_ERROR);
            }
            if (allowedMethods.contains("GET"))
                allowedMethods.add("HEAD");
            allowedMethods.add("OPTIONS");
            StringBuilder allow = new StringBuilder();
            for (int i = 0; i < allowedMethods.size(); i++) {
                allow.append(allowedMethods.get(i));
                if (i != allowedMethods.size() - 1)
                    allow.append(", ");
            }
            res.setHeader("Allow", allow.toString());
            if (request.getMethod() == Method.OPTIONS) {
                res.setStatusCode(StatusCode.OK);
                res.set("allow", allowedMethods);
            } else if (!did && !(request.getMethod() == Method.GET || request.getMethod() == Method.HEAD)) {
                res.setStatusCode(StatusCode.METHOD_NOT_ALLOWED);
            }
        }

        return res;
    }

    @Override
    public String[] getHosts() {
        return hosts;
    }

    public void addEndpoint(Method method, String path, ApiRunnable runnable) {
        if (path.charAt(0) == '/')
            path = path.substring(1);
        List<String> args = Arrays.asList(path.split("/"));
        if (!endpoints.containsKey(method))
            endpoints.put(args, new ArrayList<>());
        endpoints.get(args).add(new ApiEndpoint(method, runnable));
    }

    public void get(String p, ApiRunnable r) {
        addEndpoint(Method.GET, p, r);
    }

    public void post(String p, ApiRunnable r) {
        addEndpoint(Method.POST, p, r);
    }

    public void head(String p, ApiRunnable r) {
        addEndpoint(Method.HEAD, p, r);
    }

    public void delete(String p, ApiRunnable r) {
        addEndpoint(Method.DELETE, p, r);
    }

    public void put(String p, ApiRunnable r) {
        addEndpoint(Method.PUT, p, r);
    }

    public void patch(String p, ApiRunnable r) {
        addEndpoint(Method.PATCH, p, r);
    }

    /**
     * Set whether or not the User-Agent header is required, by default this is true.
     *
     * @param userAgentReqired If user agent is required
     */
    public void setUserAgentReqired(boolean userAgentReqired) {
        this.userAgentReqired = userAgentReqired;
    }

    public boolean isUserAgentReqired() {
        return userAgentReqired;
    }
}
