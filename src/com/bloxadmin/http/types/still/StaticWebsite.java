package com.bloxadmin.http.types.still;

import com.bloxadmin.http.Request;
import com.bloxadmin.http.Response;
import com.bloxadmin.http.Website;
import com.bloxadmin.http.StatusCode;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;

public class StaticWebsite implements Website {

    private final String[] hosts;
    private final File directory;
    private HashMap<Integer, File> errorPages = new HashMap<>();

    public StaticWebsite(File directory, String... hosts) {
        this.hosts = hosts;
        if (directory.isFile())
            directory = directory.getParentFile();
        this.directory = directory;
    }

    @Override
    public Response handle(Request req) throws IOException {
        Response res = new Response();
        res.setStatusCode(StatusCode.OK);

        File file = new File(directory, req.getRequestUri().getPath());
        if (!file.exists()) {
            res.setStatusCode(StatusCode.NOT_FOUND);
        } else if (file.isDirectory()) {
            file = new File(file, "index.html");
            if (!file.exists()) {
                res.setStatusCode(StatusCode.NOT_FOUND);
            }
        }

        if (res.getStatusCode() != StatusCode.OK) {
            file = errorPages.get(res.getStatusCode().getCode());
            onError(res.getStatusCode().getCode(), req, res, file);
            if (file == null)
                return res;
        }

        res.setHeader("Content-Type", Files.probeContentType(file.toPath()));
        res.setBody(readFile(file));
        return res;
    }

    @Override
    public String[] getHosts() {
        return hosts;
    }

    private static byte[] readFile(File file) throws IOException {
        return Files.readAllBytes(file.toPath());
    }

    public void setErrorPage(int errorCode, File page) {
        if (errorCode >= 600 || errorCode < 400)
            return;
        if (!page.exists())
            return;
        if (!page.isFile())
            return;
        errorPages.put(errorCode, page);
    }

    public void setErrorPage(int errorCode, String page) {
        setErrorPage(errorCode, new File(directory, page));
    }

    public void setErrorPage(StatusCode errorCode, File page) {
        setErrorPage(errorCode.getCode(), page);
    }

    public void setErrorPage(StatusCode errorCode, String page) {
        setErrorPage(errorCode.getCode(), new File(directory, page));
    }

    public void onError(int code, Request req, Response res, File errorPage) {

    }

    public File getDirectory() {
        return directory;
    }
}
