package com.bloxadmin.http;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParser;

import java.io.*;
import java.net.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * HTTP Request from a client
 */
public class Request {

    private static final int MAX_HEADER_SIZE = 100;

    private Map<String, String> headers = new HashMap<>();
    private URI uri;
    private Method method;
    private String body;
    private Socket socket;
    private ServerSocket serverSocket;

    /**
     * Parses socket and creates a request object
     *
     * @param serverSocket Socket the server is listening on
     * @param socket       Socket the client is connected to
     * @throws IOException        If there is an error in the input
     * @throws URISyntaxException If requester sent an invalid host or path
     */
    public Request(ServerSocket serverSocket, Socket socket) throws IOException, URISyntaxException {
        this.socket = socket;
        this.serverSocket = serverSocket;
        InputStream inputStream = socket.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        // Read first line to see if it is an actual HTTP request
        // The format for the first line should be:
        // METHOD /path/to/resource HTTP/1.1

        String firstLine = reader.readLine();
        if (firstLine == null)
            throw new IOException("Invalid HTTP request");
        String[] flParts = firstLine.split(" ");
        if (flParts.length > 3) {
            throw new IOException();
        } else if (!flParts[2].toUpperCase().equals("HTTP/1.1")) {
            // TODO: Tell user of unsupported protocol
            throw new IOException();
        }

        method = Method.fromString(flParts[0]);
        String path = flParts[1];

        String line;
        for (int i = 0; i < MAX_HEADER_SIZE && reader.ready(); i++) {
            line = reader.readLine();
            if (line == null)
                break;
            if (line.trim().equals(""))
                break;
            int index = line.indexOf(':');
            if (index == -1 || line.trim().length() <= index)
                continue;
            if (line.substring(index + 1).trim().equals(""))
                continue;
            String k = line.substring(0, index).trim().toLowerCase();
            String v = line.substring(index + 1).trim();
            addHeader(k, v);
        }

        String url = "http://" + getHeader("host") + path;
        uri = new URL(url).toURI();

        // StringBuilder bodyBuilder = new StringBuilder();
        String contentLengthHeader = getHeader("content-length");
        if (contentLengthHeader != null) {
            int contentLength = Integer.parseInt(contentLengthHeader);
            char[] buffer = new char[contentLength];
            reader.read(buffer, 0, contentLength);
            // while ((line = reader.readLine()) != null) {
            //     bodyBuilder.append("\n").append(line);
            // }
            // body = bodyBuilder.toString();
            body = new String(buffer);
        }


        System.out.println(socket.getRemoteSocketAddress().toString() + " <" + method.getName() + "> " + getHeader("host") + path);
    }

    private void addHeader(String header, Object value) {
        header = header.toLowerCase();
        headers.put(header, value.toString());
    }

    /**
     * Get all headers
     *
     * @return map of headers
     */
    public Map<String, String> getHeaders() {
        return headers;
    }

    /**
     * Get request header
     *
     * @param header key
     * @return value
     */
    public String getHeader(String header) {
        return headers.get(header.toLowerCase());
    }

    /**
     * Get requested URI
     *
     * @return URI
     */
    public URI getRequestUri() {
        return uri;
    }

    /**
     * Get request method
     *
     * @return method
     */
    public Method getMethod() {
        return method;
    }

    /**
     * Get request body
     *
     * @return body
     */
    public String getBody() {
        return body;
    }

    /**
     * Parse the url query
     *
     * @return Map with data
     */
    public HashMap<String, String> getQuery() {
        HashMap<String, String> map = new HashMap<>();
        if (getRequestUri().getRawQuery() == null)
            return map;
        String[] parts = getRequestUri().getRawQuery().split("&");
        for (String part : parts) {
            try {
                String[] s = part.split("=");
                if (s.length < 2)
                    continue;
                String name = URLDecoder.decode(s[0], "UTF-8");
                String content = URLDecoder.decode(s[1], "UTF-8");
                map.put(name, content);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    /**
     * Parses body into a JsonElement. Returns JsonNull if mime type is incorrect.
     *
     * @return JsonElement of body
     */
    public JsonElement getJsonBody() {
        return getJsonBody(true);
    }

    /**
     * Parses body into a JsonElement.
     *
     * @param safe If safe mode is enabled, will return JsonNull on invalid mime type.
     * @return JsonElement of body
     */
    public JsonElement getJsonBody(boolean safe) {
        String type = getHeader("content-type").toLowerCase().trim();
        if (safe)
            if (!(type.startsWith("application/json") || type.startsWith("text/json")))
                return JsonNull.INSTANCE;
        return new JsonParser().parse(getBody());
    }

    /**
     * Get form data from request, parses URL Form Encoded or Multipart Form Data
     *
     * @return Map with data
     */
    public HashMap<String, String> getForm() {
        String type = getHeader("content-type").toLowerCase().trim();
        if (type.startsWith("application/x-www-form-urlencoded"))
            return getFormURLEncoded();
        if (type.startsWith("multipart/form-data"))
            return getMultipartFormData();
        return new HashMap<>();
    }

    /**
     * Parses body as FormURLEncoded
     *
     * @return Map with data
     */
    public HashMap<String, String> getFormURLEncoded() {
        HashMap<String, String> map = new HashMap<>();
        if (!getHeader("content-type").toLowerCase().startsWith("application/x-www-form-urlencoded"))
            return map;
        String[] parts = getBody().split("&");
        for (String part : parts) {
            try {
                String[] s = part.split("=");
                if (s.length < 2)
                    continue;
                String name = URLDecoder.decode(s[0], "UTF-8");
                String content = URLDecoder.decode(s[1], "UTF-8");
                map.put(name, content);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    /**
     * Parses body as a Multipart Form Data
     *
     * @return Map with data
     */
    public HashMap<String, String> getMultipartFormData() {
        HashMap<String, String> map = new HashMap<>();
        String[] contentType = getHeader("content-type").split(";");
        if (contentType.length < 2)
            return map;
        if (!contentType[0].trim().toLowerCase().equals("multipart/form-data"))
            return map;
        if (!contentType[1].trim().toLowerCase().startsWith("boundary="))
            return map;
        String boundary = contentType[1].trim().substring(9).trim();
        List<String> parts = Arrays.asList(getBody().split("--" + boundary));

        for (String part : parts) {
            if (part.trim().equals(""))
                continue;
            if (part.equals("--\r\n"))
                continue;
            if (!part.startsWith("\r\nContent-Disposition: form-data; name=\""))
                continue;
            String[] lines = part.substring(2, part.length() - 2).split("\r\n");
            if (lines.length < 2)
                continue;
            String name = lines[0].substring(38, lines[0].length() - 1);
            StringBuilder content = new StringBuilder();
            for (int i = 2; i < lines.length; i++) {
                content.append(lines[i]).append("\n");
            }
            content.delete(content.length() - 1, content.length());

            map.put(name, content.toString());
        }

        return map;
    }

    /**
     * Get the socket the server is listening on
     *
     * @return ServerSocket
     */
    public ServerSocket getServerSocket() {
        return serverSocket;
    }

    /**
     * Get the socket that is connected to the client
     *
     * @return Client socket
     */
    public Socket getSocket() {
        return socket;
    }
}
