package com.bloxadmin.http;

import java.io.IOException;

public interface Website<P extends Response> {

    P handle(Request request) throws IOException;

    String[] getHosts();
}
