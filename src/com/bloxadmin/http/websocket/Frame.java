package com.bloxadmin.http.websocket;

public class Frame {

    private final boolean fin;
    private final boolean rsv1;
    private final boolean rsv2;
    private final boolean rsv3;
    private final int opcode;
    private final byte[] payload;

    public Frame(boolean fin, boolean rsv1, boolean rsv2, boolean rsv3, int opcode, byte[] payload) {
        this.fin = fin;
        this.rsv1 = rsv1;
        this.rsv2 = rsv2;
        this.rsv3 = rsv3;
        this.opcode = opcode;
        this.payload = payload;
    }

    @Override
    public String toString() {
        return new String(payload);
    }
}
